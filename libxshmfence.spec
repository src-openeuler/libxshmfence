Name:           libxshmfence
Version:        1.3.3
Release:        1
Summary:        X11 shared memory fences
License:        MIT
URL:            https://www.x.org/
Source0:        https://www.x.org/releases/individual/lib/%{name}-%{version}.tar.xz
BuildRequires: make gcc
BuildRequires: pkgconfig(xproto)

%description
Shared memory fences for X11, as used in DRI3.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for %{name}.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --disable-static
%make_build

%check
%make_build check

%install
%make_install
%delete_la

%files
%license COPYING
%{_libdir}/*.so.*

%files devel
%{_includedir}/X11/xshmfence.h
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so

%files help
%doc README.md ChangeLog

%changelog
* Mon Dec 16 2024 Funda Wang <fundawang@yeah.net> - 1.3.3-1
- update to 1.3.3

* Sat Feb 04 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 1.3.2-1
- update to 1.3.2

* Fri Nov 04 2022 wangkerong <wangkerong@h-partners.com> - 1.3-9
- disable static library

* Wed Oct 26 2022 zhouwenpei <zhouwenpei1@h-partners.com> - 1.3-8
- Rebuild for next release

* Wed Apr 6 2022 liuyumeng1 <liuyumeng5@h-partners.com> - 1.3-7
- enable tests

* Sat Oct 19 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3-6
- Type:bugfix
- Id:NA
- SUG:NA
- DESC:change the directory of the license file and add the help package for libxshmfence

* Wed Oct 09 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3-5
- Type:enhancement
- ID:NA
- SUG:NA
  DESC:add COPYING

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.3-4
- Package init
